/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2022-2022  Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <sys/time.h>
#include <cblas-openblas.h>
#include <lapacke.h>

#include "cholesky.h"

struct s_tile
{
	double *data;
	int tile_row;
	int tile_col;
	int nb_rows;
	int nb_cols;
	int ld;
};

void disp_tile(struct s_tile *tile)
{
	printf("tile(row=%d, col=%d)\n", tile->tile_row, tile->tile_col);
	int row;
	for (row=0; row<tile->nb_rows; row++)
	{
		int col;
		for (col=0; col<tile->nb_cols; col++)
		{
			printf(" %3le", tile->data[_(row, col, tile->ld)]);
		}
		printf("\n");
	}
}

struct s_tile *get_tile(double *A,
			int tile_row, int tile_col,
			int nb_rows, int nb_cols, int ld_A, int bs_A)
{
	double *data = calloc(nb_rows*nb_cols, sizeof(*data));

	const int tile_ld = nb_rows;
	int col;
	for (col=0; col<nb_cols; col++)
	{
		memcpy(data + _(0, col, tile_ld),
		       A + _(tile_row*bs_A, tile_col*bs_A + col, ld_A),
		       nb_rows*sizeof(*data));
	}

	struct s_tile *tile = calloc(1, sizeof(*tile));
	tile->data = data;
	tile->tile_row = tile_row;
	tile->tile_col = tile_col;
	tile->nb_rows = nb_rows;
	tile->nb_cols = nb_cols;
	tile->ld = tile_ld;

	return tile;
}

void put_tile(struct s_tile *tile, double *A, int ld_A, int bs_A)
{
	int col;
	for (col=0; col<tile->nb_cols; col++)
	{
		memcpy(A + _(tile->tile_row*bs_A, tile->tile_col*bs_A + col, ld_A),
		       tile->data + _(0, col, tile->ld),
		       tile->nb_rows*sizeof(*tile->data));
	}

	free(tile->data);
	free(tile);
}

struct s_tile **get_tiles(double *A, int n, int ld_A, int bs_A)
{
	const int rem              = n % bs_A;
	const int n_tiles          = rem>0 ? n/bs_A + 1 : n/bs_A;
	const int last_bs_A        = rem>0 ? rem : bs_A;
	struct s_tile **tile_array = calloc(n_tiles*n_tiles, sizeof(*tile_array));

	int tile_col;
	for (tile_col=0; tile_col<n_tiles; tile_col++)
	{
		int tile_nb_cols = (tile_col+1 < n_tiles) ? bs_A : last_bs_A;
		int tile_row;
		for (tile_row=0; tile_row<n_tiles; tile_row++)
		{
			int tile_nb_rows = (tile_row+1 < n_tiles) ? bs_A : last_bs_A;
			tile_array[_(tile_row, tile_col, n_tiles)] =
				get_tile(A, tile_row, tile_col, tile_nb_rows, tile_nb_cols, ld_A, bs_A);

		}
	}
	return tile_array;
}

void put_tiles(struct s_tile **tile_array, double *A, int n, int ld_A, int bs_A)
{
	const int rem     = n % bs_A;
	const int n_tiles = rem>0 ? n/bs_A + 1 : n/bs_A;

	int tile_col;
	for (tile_col=0; tile_col<n_tiles; tile_col++)
	{
		int tile_row;
		for (tile_row=0; tile_row<n_tiles; tile_row++)
		{
			put_tile(tile_array[_(tile_row, tile_col, n_tiles)], A, ld_A, bs_A);
			tile_array[_(tile_row, tile_col, n_tiles)] = NULL;
		}
	}
	free(tile_array);
}

static void func_potrf(struct s_tile *tile_A)
{
	const int n      = tile_A->nb_rows;
	double * const A = tile_A->data;
	const int lda    = tile_A->ld;
	int ret;

	ret = LAPACKE_dpotrf(LAPACK_COL_MAJOR, 'L', n, A, lda);
	assert(ret == 0);
}

static void func_trsm(struct s_tile *tile_A, struct s_tile *tile_B)
{
	const int order          = CblasColMajor;
	const int side           = CblasRight;
	const int uplo           = CblasLower;
	const int transA         = CblasTrans;
	const int diag           = CblasNonUnit;
	const int m              = tile_B->nb_rows;
	const int n              = tile_B->nb_cols;
	const double alpha       = 1.0;
	const double * const A   = tile_A->data;
	const int lda            = tile_A->ld;
	double * const B         = tile_B->data;
	const int ldb            = tile_B->ld;

	cblas_dtrsm(order, side, uplo, transA, diag, m, n, alpha, A, lda, B, ldb);
}

static void func_syrk(struct s_tile *tile_A, struct s_tile *tile_C)
{
	const int order        = CblasColMajor;
	const int uplo         = CblasLower;
	const int transA       = CblasNoTrans;
	const int n            = tile_A->nb_rows;
	const int k            = tile_A->nb_cols;
	const double alpha     = -1.0;
	const double * const A = tile_A->data;
	const int lda          = tile_A->ld;
	const double beta      = 1.0;
	double * const C       = tile_C->data;
	const int ldc          = tile_C->ld;

	cblas_dsyrk(order, uplo, transA, n, k, alpha, A, lda, beta, C, ldc);
}

static void func_gemm(struct s_tile *tile_A, struct s_tile *tile_B, struct s_tile *tile_C)
{
	const int order        = CblasColMajor;
	const int transA       = CblasNoTrans;
	const int transB       = CblasTrans;
	const int m            = tile_A->nb_rows;
	const int n            = tile_B->nb_cols;
	const int k            = tile_A->nb_cols;
	const double alpha     = -1.0;
	const double * const A = tile_A->data;
	const int lda          = tile_A->ld;
	const double * const B = tile_B->data;
	const int ldb          = tile_B->ld;
	const double beta      = 1.0;
	double * const C       = tile_C->data;
	const int ldc          = tile_C->ld;

	cblas_dgemm(order, transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
}

static double cholesky(double * const __restrict__ A, const int n, const int bs)
{
	const int rem = n%bs;
	const int nb  = rem>0?n/bs+1:n/bs;

	int k;
	struct timeval begin, end;

	struct s_tile **tile_array = get_tiles(A, n, n, bs);

	gettimeofday(&begin, NULL);
	for (k = 0; k < nb; k++)
	{
		func_potrf(tile_array[_(k,k,nb)]);

		int m;
		for (m=k+1; m<nb; m++)
		{
			func_trsm(tile_array[_(k,k,nb)], tile_array[_(m,k,nb)]);
		}

		for (m=k+1; m<nb; m++)
		{
			func_syrk(tile_array[_(m,k,nb)], tile_array[_(m,m,nb)]);

			int n;
			for (n=k+1; n<m; n++)
			{
				func_gemm(tile_array[_(m,k,nb)], tile_array[_(n,k,nb)], tile_array[_(m,n,nb)]);
			}
		}
	}
	gettimeofday(&end, NULL);

	put_tiles(tile_array, A, n, n, bs);
	return (double)((end.tv_sec - begin.tv_sec)*1000000 + (end.tv_usec - begin.tv_usec));
}

int main(int argc, char *argv[])
{
	int verbose;
	int n;
	int bs;

	read_params(argc, argv, &verbose, &n, &bs);

	double *A = calloc(n*n, sizeof(*A));
	assert(A != NULL);
	fill(A, n, n);

	double *refA = malloc(n*n*sizeof(*refA));
	assert(refA != NULL);
	memcpy(refA, A, n*n*sizeof(*refA));

	if (verbose)
	{
		disp(A, n, n, "Orig A:");
	}

	double timing = cholesky(A, n, bs);
	ref_cholesky(refA, n, n);

	if (verbose)
	{
		disp(A, n, n, "Chol A:");
		disp(refA, n, n, "Ref Chol A:");
	}

	check(A, refA, n, verbose);
	free(refA);
	free(A);

	{
		printf("#size,tile,time\n");
		printf("%d,%d,%.2lf\n", n, bs, timing/1e6 /* us -> s */);
	}

	return 0;
}
