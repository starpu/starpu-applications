/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2022-2022  Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */

struct starpu_perfmodel model_potrf =
{
	.symbol = "potrf",
	.type = STARPU_HISTORY_BASED,
};
struct starpu_perfmodel model_trsm =
{
	.symbol = "trsm",
	.type = STARPU_HISTORY_BASED,
};
struct starpu_perfmodel model_syrk =
{
	.symbol = "syrk",
	.type = STARPU_HISTORY_BASED,
};
struct starpu_perfmodel model_gemm =
{
	.symbol = "gemm",
	.type = STARPU_HISTORY_BASED,
};

static void func_potrf(void *buffers[], void *cl_args);
static void func_trsm(void *buffers[], void *cl_args);
static void func_syrk(void *buffers[], void *cl_args);
static void func_gemm(void *buffers[], void *cl_args);

#ifdef USE_CUDA
static void cuda_func_trsm(void *buffers[], void *cl_args);
static void cuda_func_syrk(void *buffers[], void *cl_args);
static void cuda_func_gemm(void *buffers[], void *cl_args);
#endif

struct starpu_codelet cl_potrf =
{
	.name = "POTRF",
	.cpu_funcs = {func_potrf},
	.nbuffers = 1,
	.model = &model_potrf,
	.modes = {STARPU_RW}
};

struct starpu_codelet cl_trsm =
{
	.name = "TRSM",
	.cpu_funcs = {func_trsm},
#ifdef USE_CUDA
	.cuda_funcs = {cuda_func_trsm},
	.cuda_flags = {STARPU_CUDA_ASYNC},
#endif
	.nbuffers = 2,
	.model = &model_trsm,
	.modes = {STARPU_R, STARPU_RW}
};

struct starpu_codelet cl_syrk =
{
	.name = "SYRK",
	.cpu_funcs = {func_syrk},
#ifdef USE_CUDA
	.cuda_funcs = {cuda_func_syrk},
	.cuda_flags = {STARPU_CUDA_ASYNC},
#endif
	.nbuffers = 2,
	.model = &model_syrk,
	.modes = {STARPU_R, STARPU_RW}
};

struct starpu_codelet cl_gemm =
{
	.name = "GEMM",
	.cpu_funcs = {func_gemm},
#ifdef USE_CUDA
	.cuda_funcs = {cuda_func_gemm},
	.cuda_flags = {STARPU_CUDA_ASYNC},
#endif
	.nbuffers = 3,
	.model = &model_gemm,
	.modes = {STARPU_R, STARPU_R, STARPU_RW}
};

static void func_potrf(void *buffers[], void *cl_args)
{
	double * const A = (double *)(double *)STARPU_MATRIX_GET_PTR(buffers[0]);
	const int n = STARPU_MATRIX_GET_NX(buffers[0]);
	const int lda = STARPU_MATRIX_GET_LD(buffers[0]);
	int ret = LAPACKE_dpotrf(LAPACK_COL_MAJOR, 'L', n, A, lda);
	assert(ret == 0);
}

static void func_trsm(void *buffers[], void *cl_args)
{
	const int order          = CblasColMajor;
	const int side           = CblasRight;
	const int uplo           = CblasLower;
	const int transA         = CblasTrans;
	const int diag           = CblasNonUnit;
	const int m              = STARPU_MATRIX_GET_NX(buffers[1]); /* number of B rows */
	const int n              = STARPU_MATRIX_GET_NY(buffers[1]); /* number of B cols */
	const double alpha       = 1.0;
	const double * const A   = (double *)STARPU_MATRIX_GET_PTR(buffers[0]);
	const int lda            = STARPU_MATRIX_GET_LD(buffers[0]);
	double * const B         = (double *)STARPU_MATRIX_GET_PTR(buffers[1]);
	const int ldb            = STARPU_MATRIX_GET_LD(buffers[1]);

	cblas_dtrsm(order, side, uplo, transA, diag, m, n, alpha, A, lda, B, ldb);
}

static void func_syrk(void *buffers[], void *cl_args)
{
	const int order        = CblasColMajor;
	const int uplo         = CblasLower;
	const int transA       = CblasNoTrans;
	const int n            = STARPU_MATRIX_GET_NX(buffers[0]);
	const int k            = STARPU_MATRIX_GET_NY(buffers[0]);
	const double alpha     = -1.0;
	const double * const A = (double *)STARPU_MATRIX_GET_PTR(buffers[0]);
	const int lda          = STARPU_MATRIX_GET_LD(buffers[0]);
	const double beta      = 1.0;
	double * const C       = (double *)STARPU_MATRIX_GET_PTR(buffers[1]);
	const int ldc          = STARPU_MATRIX_GET_LD(buffers[1]);

	cblas_dsyrk(order, uplo, transA, n, k, alpha, A, lda, beta, C, ldc);
}

static void func_gemm(void *buffers[], void *cl_args)
{
	const int order        = CblasColMajor;
	const int transA       = CblasNoTrans;
	const int transB       = CblasTrans;
	const int m            = STARPU_MATRIX_GET_NX(buffers[0]);
	const int n            = STARPU_MATRIX_GET_NY(buffers[1]);
	const int k            = STARPU_MATRIX_GET_NY(buffers[0]);
	const double alpha     = -1.0;
	const double * const A = (double *)STARPU_MATRIX_GET_PTR(buffers[0]);
	const int lda          = STARPU_MATRIX_GET_LD(buffers[0]);
	const double * const B = (double *)STARPU_MATRIX_GET_PTR(buffers[1]);
	const int ldb          = STARPU_MATRIX_GET_LD(buffers[1]);
	const double beta      = 1.0;
	double * const C       = (double *)STARPU_MATRIX_GET_PTR(buffers[2]);
	const int ldc          = STARPU_MATRIX_GET_LD(buffers[2]);

	cblas_dgemm(order, transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
}

#ifdef USE_CUDA
static void cuda_func_trsm(void *buffers[], void *cl_args)
{
	const int side           = CUBLAS_SIDE_RIGHT;
	const int uplo           = CUBLAS_FILL_MODE_LOWER;
	const int transA         = CUBLAS_OP_T;
	const int diag           = CUBLAS_DIAG_NON_UNIT;
	const int m              = STARPU_MATRIX_GET_NX(buffers[1]); /* number of B rows */
	const int n              = STARPU_MATRIX_GET_NY(buffers[1]); /* number of B cols */
	const double alpha       = 1.0;
	const double * const A   = (double *)STARPU_MATRIX_GET_PTR(buffers[0]);
	const int lda            = STARPU_MATRIX_GET_LD(buffers[0]);
	double * const B         = (double *)STARPU_MATRIX_GET_PTR(buffers[1]);
	const int ldb            = STARPU_MATRIX_GET_LD(buffers[1]);

	cublasStatus_t status = cublasDtrsm(starpu_cublas_get_local_handle(),
					    side, uplo, transA, diag, m, n, &alpha, A, lda, B, ldb);
	if (status != CUBLAS_STATUS_SUCCESS)
		STARPU_CUBLAS_REPORT_ERROR(status);
}

static void cuda_func_syrk(void *buffers[], void *cl_args)
{
	const int uplo         = CUBLAS_FILL_MODE_LOWER;
	const int transA       = CUBLAS_OP_N;
	const int n            = STARPU_MATRIX_GET_NX(buffers[0]);
	const int k            = STARPU_MATRIX_GET_NY(buffers[0]);
	const double alpha     = -1.0;
	const double * const A = (double *)STARPU_MATRIX_GET_PTR(buffers[0]);
	const int lda          = STARPU_MATRIX_GET_LD(buffers[0]);
	const double beta      = 1.0;
	double * const C       = (double *)STARPU_MATRIX_GET_PTR(buffers[1]);
	const int ldc          = STARPU_MATRIX_GET_LD(buffers[1]);

	cublasStatus_t status = cublasDsyrk(starpu_cublas_get_local_handle(),
					    uplo, transA, n, k, &alpha, A, lda, &beta, C, ldc);
	if (status != CUBLAS_STATUS_SUCCESS)
		STARPU_CUBLAS_REPORT_ERROR(status);
}

static void cuda_func_gemm(void *buffers[], void *cl_args)
{
	const int transA       = CUBLAS_OP_N;
	const int transB       = CUBLAS_OP_T;
	const int m            = STARPU_MATRIX_GET_NX(buffers[0]);
	const int n            = STARPU_MATRIX_GET_NY(buffers[1]);
	const int k            = STARPU_MATRIX_GET_NY(buffers[0]);
	const double alpha     = -1.0;
	const double * const A = (double *)STARPU_MATRIX_GET_PTR(buffers[0]);
	const int lda          = STARPU_MATRIX_GET_LD(buffers[0]);
	const double * const B = (double *)STARPU_MATRIX_GET_PTR(buffers[1]);
	const int ldb          = STARPU_MATRIX_GET_LD(buffers[1]);
	const double beta      = 1.0;
	double * const C       = (double *)STARPU_MATRIX_GET_PTR(buffers[2]);
	const int ldc          = STARPU_MATRIX_GET_LD(buffers[2]);

	cublasStatus_t status = cublasDgemm(starpu_cublas_get_local_handle(),
					    transA, transB, m, n, k, &alpha, A, lda, B, ldb, &beta, C, ldc);
	if (status != CUBLAS_STATUS_SUCCESS)
		STARPU_CUBLAS_REPORT_ERROR(status);
}
#endif

