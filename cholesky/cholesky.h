/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2022-2022  Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */

#include "utils.h"

#define DEFAULT_SIZE 1024
#define DEFAULT_TILE_SIZE 128

static void fill(double *A, int n, int lda)
{
	int ret;
	int i;
	int seed[] = {0,0,0,1};

	ret = LAPACKE_dlarnv(1 /* uniform 0..1 */, seed, n*lda, A);
	assert(ret == 0);
	for (i=0; i<n; i++)
	{
		int j;
		A[_(i,i,lda)] += n;
		for (j=i+1; j<n; j++)
		{
			A[_(i,j,lda)] = 0;
		}
	}
}

static void ref_cholesky(double *refA, int n, int lda)
{
	int ret = LAPACKE_dpotrf(LAPACK_COL_MAJOR, 'L', n, refA, lda);
	assert(ret == 0);
}

static void check(double *A, double *refA, int n, int verbose)
{
	const double tol = 50.0 * LAPACKE_dlamch('E');
	cblas_daxpy(n*n, -1.0, refA, 1, A, 1);
	double Anorm = LAPACKE_dlansy(CblasColMajor, 'F', CblasLower, n, refA, n);
	double error = LAPACKE_dlange(CblasColMajor, 'F', n, n, A, n);
	if (Anorm != 0)
		error /= Anorm;
	if (verbose)
	{
		printf("error level = %lf\n", error);
	}
	if (fabs(error) > tol)
	{
		fprintf(stderr, "check failed\n");
		assert(1);
	}
}

static void read_params(int argc, char **argv, int *verbose, int *n, int *bs)
{
	*verbose = 0;
	*n = DEFAULT_SIZE;
	*bs = DEFAULT_TILE_SIZE;
	if (argc < 3)
	{
		fprintf(stderr, "usage: %s [-v] <N> <BS>\n", argv[0]);
	}
	int pos = 1;
	if (argv[pos] == NULL) return;

	if (strcmp(argv[pos], "-v") == 0)
	{
		*verbose = 1;
		pos ++;
	}
	if (argv[pos] == NULL) return;
	*n = atoi(argv[pos]);
	if (*n < 1)
	{
		fprintf(stderr, "N must be >= 1\n");
		exit(1);
	}
	if (argv[pos+1] == NULL) return;
	*bs = atoi(argv[pos+1]);
	if (*bs < 1)
	{
		fprintf(stderr, "BS must be >= 1\n");
		exit(1);
	}
}
