/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2022-2022  Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */

#include <starpu_mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <lapacke.h>
#ifdef USE_CUDA
#include <starpu_cublas_v2.h>
#endif
// include that file at the end to avoid problems with gnu_define
#include <cblas-openblas.h>

#include "cholesky.h"
#include "cholesky_starpu.h"

static double cholesky(double * const __restrict__ A, const int n, const int lda, const int bs, const int comm_size)
{
	const int rem = n%bs;
	const int nb = rem>0?n/bs+1:n/bs;
	const int last_bs = rem>0?rem:bs;
	starpu_data_handle_t *h_A = malloc(nb*nb*sizeof(*h_A));

	starpu_mpi_tag_t mpi_tag = 0;

	int k;
	for (k = 0; k < nb; k++)
	{
		const int k_bs = k+1<nb?bs:last_bs;
		int m;
		for (m = k; m < nb; m++)
		{
			const int m_bs = m+1<nb?bs:last_bs;
			/* matrix is stored column-major:
			 * x = nb_rows = m_bs
			 * y = nb_cols = k_bs */
			starpu_matrix_data_register(&h_A[_(m,k,nb)], STARPU_MAIN_RAM, (uintptr_t)&A[_(m*bs,k*bs,lda)], lda, m_bs, k_bs, sizeof(*A));
			starpu_mpi_data_register(h_A[_(m,k,nb)], mpi_tag++, ((m*nb)+k)%comm_size);
		}
	}

	double start = starpu_timing_now();
	starpu_fxt_start_profiling();
	for (k = 0; k < nb; k++)
	{
		int ret;

		ret = starpu_mpi_task_insert(MPI_COMM_WORLD, &cl_potrf,
					     STARPU_RW, h_A[_(k,k,nb)],
					     STARPU_PRIORITY, STARPU_MAX_PRIO,
					     0);
		STARPU_CHECK_RETURN_VALUE(ret, "starpu_mpi_task_insert");

		int m;
		for (m=k+1; m<nb; m++)
		{
			ret = starpu_mpi_task_insert(MPI_COMM_WORLD, &cl_trsm,
						     STARPU_R, h_A[_(k,k,nb)],
						     STARPU_RW, h_A[_(m,k,nb)],
						     STARPU_PRIORITY, m==k+1?STARPU_MAX_PRIO:STARPU_DEFAULT_PRIO,
						     0);
			STARPU_CHECK_RETURN_VALUE(ret, "starpu_mpi_task_insert");
		}

		for (m=k+1; m<nb; m++)
		{
			ret = starpu_mpi_task_insert(MPI_COMM_WORLD, &cl_syrk,
						     STARPU_R, h_A[_(m,k,nb)],
						     STARPU_RW, h_A[_(m,m,nb)],
						     STARPU_PRIORITY, m==k+1?STARPU_MAX_PRIO:STARPU_DEFAULT_PRIO,
						     0);
			STARPU_CHECK_RETURN_VALUE(ret, "starpu_mpi_task_insert");

			int n;
			for (n=k+1; n<m; n++)
			{
				ret = starpu_mpi_task_insert(MPI_COMM_WORLD, &cl_gemm,
							     STARPU_R, h_A[_(m,k,nb)],
							     STARPU_R, h_A[_(n,k,nb)],
							     STARPU_RW, h_A[_(m,n,nb)],
							     STARPU_PRIORITY, n==k+1?STARPU_MAX_PRIO:STARPU_DEFAULT_PRIO,
							     0);
				STARPU_CHECK_RETURN_VALUE(ret, "starpu_mpi_task_insert");
			}
		}
	}
	starpu_task_wait_for_all();
	starpu_fxt_stop_profiling();

	double end = starpu_timing_now();

	for (k = 0; k < nb; k++)
	{
		int m;
		for (m = k; m < nb; m++)
		{
			/* Get back data on node 0 for the check */
			starpu_mpi_get_data_on_node(MPI_COMM_WORLD, h_A[_(m,k,nb)], 0);
			starpu_data_unregister(h_A[_(m,k,nb)]);
		}
	}
	free(h_A);

	return end-start;
}

int main(int argc, char *argv[])
{
	int verbose;
	int n;
	int bs;
	int comm_rank, comm_size;
	int ret;

	read_params(argc, argv, &verbose, &n, &bs);

	setenv("STARPU_MPI_STATS", "1", 1);
	setenv("STARPU_SCHED", "dmda", 0);

	ret = starpu_mpi_init_conf(&argc, &argv, 1, MPI_COMM_WORLD, NULL);
	STARPU_CHECK_RETURN_VALUE(ret, "starpu_mpi_ini_conf");
	starpu_mpi_comm_rank(MPI_COMM_WORLD, &comm_rank);
	starpu_mpi_comm_size(MPI_COMM_WORLD, &comm_size);

	starpu_cublas_init();

	double *A = calloc(n*n, sizeof(*A));
	starpu_memory_pin(A, n*n*sizeof(*A));
	assert(A != NULL);
	fill(A, n, n);

	double *refA = malloc(n*n*sizeof(*refA));
	assert(refA != NULL);
	memcpy(refA, A, n*n*sizeof(*refA));

	if (verbose)
	{
		disp(A, n, n, "Orig A:");
	}

	double timing = cholesky(A, n, n, bs, comm_size);
	ref_cholesky(refA, n, n);

	if (verbose)
	{
		disp(A, n, n, "Chol A:");
		disp(refA, n, n, "Ref Chol A:");
	}

	if (comm_rank == 0) check(A, refA, n, verbose);
	free(refA);
	free(A);

	if (comm_rank == 0)
	{
		struct starpu_sched_policy *spolicy = starpu_sched_get_sched_policy();
		const int ncpu = starpu_cpu_worker_get_count();

		printf("#policy,ncpus,size,tile,time\n");
		printf("\"%s\",%d,%d,%d,%.2lf\n", spolicy->policy_name, ncpu, n, bs, timing/1e6 /* us -> s */);
	}

	starpu_cublas_shutdown();
	starpu_mpi_shutdown();

	return 0;
}
