/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2022-2022  Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */

#define _(row,col,ld) ((row)+(col)*(ld))

static void disp(double *A, int n, int lda, const char *title)
{
	printf("%s\n", title);
	int cut = 0;
	if (n > 10)
	{
		cut = 1;
		n = 10;
	}
	int row;
	for (row=0; row<n; row++)
	{
		int col;
		for (col=0; col<n; col++)
		{
			printf(" %.2lf", A[_(row,col,lda)]);
		}
		if (cut)
		{
			printf(" ...");
		}
		printf("\n");
	}
	if (cut)
	{
		printf(" ...  ...  ...\n");
	}
}

