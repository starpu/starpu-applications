SRCS		=	$(wildcard *.c)
PRGS		=	$(SRCS:.c=)

CC		=	gcc
CFLAGS		=	-g -Wall -O0 -I../
LDFLAGS		+=	-lm
MPICC		=	mpicc

CUDA		=	$(shell starpu_config|grep 'define STARPU_USE_CUDA'|wc -l)
ifneq ($(CUDA), 0)
CFLAGS		+=	-DUSE_CUDA
LDFLAGS		+=	-lcublas
endif

CFLAGS		+=	$(shell pkg-config --cflags starpu-1.4)
LDFLAGS		+=	$(shell pkg-config --libs starpu-1.4)
MPI_LDFLAGS	+=	$(shell pkg-config --libs starpumpi-1.4)

%: %.c ../utils.h
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS) $(EXTRA_LDFLAGS)
%_mpi: %_mpi.c ../utils.h
	$(MPICC) $(CFLAGS) -o $@ $< $(LDFLAGS)  $(EXTRA_LDFLAGS) $(MPI_LDFLAGS)

.phony: all clean
all: $(PRGS)
clean:
	rm -f $(PRGS)
