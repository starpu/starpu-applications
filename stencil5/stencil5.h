/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2022-2022  Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */

#include <starpu.h>
#include <stdio.h>
#include "utils.h"

#define DEFAULT_SIZE 100
#define DEFAULT_ITER 100

static void fill(double *A, int n, int lda)
{
	starpu_srand48((long int)time(NULL));
	int i;
	for (i=0; i<n; i++)
	{
		int j;
		for (j=0; j<n; j++)
		{
			A[_(i,j,lda)] = starpu_drand48();
		}
	}
}

static void read_params(int argc, char **argv, int *verbose, int *n, int *iter)
{
	*verbose = 0;
	*n = DEFAULT_SIZE;
	*iter = DEFAULT_ITER;
	if (argc < 3)
	{
		fprintf(stderr, "usage: %s [-v] <N> <ITER>\n", argv[0]);
	}
	int pos = 1;
	if (argv[pos] == NULL) return;

	if (strcmp(argv[pos], "-v") == 0)
	{
		*verbose = 1;
		pos ++;
	}
	if (argv[pos] == NULL) return;
	*n = atoi(argv[pos]);
	if (*n < 1)
	{
		fprintf(stderr, "N must be >= 1\n");
		exit(1);
	}
	if (argv[pos+1] == NULL) return;
	*iter = atoi(argv[pos+1]);
	if (*iter < 1)
	{
		fprintf(stderr, "ITER must be >= 1\n");
		exit(1);
	}
}
