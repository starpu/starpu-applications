SUBDIRS	=	stencil5
SUBDIRS	+=	cholesky

all clean:
	@for dir in $(SUBDIRS); do (cd $$dir && ${MAKE} $@); done
